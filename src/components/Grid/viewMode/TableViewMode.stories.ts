import TableViewMode from './TableViewMode.vue';
import {gridConfiguration} from '../../../mockup/gridConfiguration'
import { Story, Meta } from '@storybook/vue/types-6-0';

export default {
  title: 'Elements/Grid/View mode/Table',
  component: TableViewMode,
} as Meta;

const Template: Story<{}> = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TableViewMode },
  template: '<TableViewMode v-bind="$props" />',
});

export const Default = Template.bind({});
Default.args = {
  columns: gridConfiguration.columns,
  data: gridConfiguration.data
};