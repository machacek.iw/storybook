import GalleryViewMode from './GalleryViewMode.vue';
import {gridConfiguration} from '../../../mockup/gridConfiguration'
import { Story, Meta } from '@storybook/vue/types-6-0';

export default {
  title: 'Elements/Grid/View mode/Gallery',
  component: GalleryViewMode,
} as Meta;

const Template: Story<{}> = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { GalleryViewMode },
  template: '<GalleryViewMode v-bind="$props" />',
});

export const Default = Template.bind({});
Default.args = {
  data: gridConfiguration.data
};