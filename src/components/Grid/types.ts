export interface Column {
    key: string;
    value: string;
}

export interface Row {
    [key: string]: string;
}

export enum ViewModes {
    table = "table",
    gallery = "gallery",
}

export interface GridProps {
    columns: Column[];
    viewModes: string[];
}