import Grid from './Grid.vue';
import * as TableViewModeStories from './viewMode/TableViewMode.stories'
import { Story, Meta } from '@storybook/vue/types-6-0';
import { ViewModes } from './types';

export default {
  title: 'Elements/Grid',
  component: Grid,
  argTypes: {
    viewModes: { control: { type: 'check', options: ViewModes } }
  },
} as Meta;

const Template: Story<{}> = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Grid },
  template: '<Grid v-bind="$props" />',
});

export const Table = Template.bind({});
Table.args = {
  ...TableViewModeStories.Default.args,
  viewModes: undefined,
};

export const Gallery = Template.bind({});
Gallery.args = {
  ...Table.args,
  viewModes: ['gallery']
};

export const AllViewModes = Template.bind({});
AllViewModes.args = {
  ...Table.args,
  viewModes: ['table', 'gallery']
};
