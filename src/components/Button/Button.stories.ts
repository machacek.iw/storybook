import MyButton from './Button.vue';
import {Sizes} from './types';
import { Story, Meta } from '@storybook/vue/types-6-0';

export default {
  title: 'Atoms/Button',
  component: MyButton,
  argTypes: {
    backgroundColor: { control: 'color' },
    // Unable recognize enum type automatically
    size: { control: { type: 'select', options: Sizes } }
  },
} as Meta;

const Template: Story = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { MyButton },
  template: '<my-button @onClick="onClick" v-bind="$props" />',
});

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'Button',
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: Sizes.sm,
  label: 'Button',
};

export const Large = Template.bind({});
Large.args = {
  size: Sizes.lg,
  label: 'Button',
};

