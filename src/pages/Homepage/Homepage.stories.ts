import Homepage from './Homepage.vue';
import { Meta } from '@storybook/vue/types-6-0';

export default {
  title: 'Pages/Homepage',
  component: Homepage,
} as Meta;

export const Default = () => ({
    components: { Homepage },
    template: '<Homepage />',
});
