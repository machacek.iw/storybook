import Vue from 'vue'
import App from './layouts/default.vue'
import VueRouter from 'vue-router'
import getRouter from '../router/';

const router = getRouter();
Vue.config.productionTip = false

Vue.use(VueRouter)
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')