import {gridData} from './gridData'

export const gridConfiguration = {
    columns: [
        {
            key: 'firstName',
            value: 'First name'
        },
        {
            key: 'lastName',
            value: 'Last name'
        },
    ],
    data: gridData,
    isStriped: true,
    galleryViewMode: true
}