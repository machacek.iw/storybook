export const gridData = [
    {
        firstName: 'Jan',
        lastName: 'Machacek',
        description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed vel lectus.'
    },
    {
        firstName: 'Petr',
        lastName: 'Novak',
        description: 'Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Pellentesque pretium lectus id turpis.'
    },
    {
        firstName: 'Tereza',
        lastName: 'Novakova',
        description: 'Molestie, porttitor ut, iaculis quis, sem. Pellentesque pretium lectus id turpis.'
    },
]