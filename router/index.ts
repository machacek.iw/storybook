import VueRouter from 'vue-router';
import Homepage from '../src/pages/Homepage/Homepage.vue';

const getRouter = () => {
  const routes = [
    {
      path: '/',
      name: 'homepage',
      components:{
          default: Homepage
      },
    },
  ];
  
  const router = new VueRouter({
    mode: 'history',
    routes,
  });
  
  return router;
};

export default getRouter;