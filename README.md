# storybook

## Storybook development
```
npm run storybook
```

## Build static files for preview
```
npm run build-storybook
```

## Synchronize chromatic with gitlab repository
```
npm run chromatic
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
